import { Component } from '@angular/core';
import { MatSnackBar } from '@angular/material';

/**
 * @title Snack-bar with a custom component
 */

@Component({
  selector: 'app-test',
  templateUrl: 'test-component.html',
})
export class TestComponent {
  constructor(public snackBar: MatSnackBar) { }

  openSnackBar() {
    this.snackBar.openFromComponent(PizzaPartyComponent, {
      duration: 500,
    });
  }
}


@Component({
  selector: 'app-test-component-snack',
  templateUrl: 'test-component-snack.html',
  styles: [`
    .example-pizza-party {
      color: hotpink;
    }
  `],
})
export class PizzaPartyComponent { }

import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';
import { MatBottomSheet, MatBottomSheetRef } from '@angular/material';
import { first } from 'rxjs/operators';
import { Event } from '../_models';
import { AlertService, EventService } from '../_services';
import { BottomBarComponent } from '../bottom-bar/bottom-bar.component';

@Component({
  selector: 'app-today',
  templateUrl: './today.component.html',
  styleUrls: ['./today.component.css']
})

export class TodayComponent implements OnInit {
  longToday: string = moment().format('dddd Do MMMM YYYY');
  today: string = moment().format('MMMM Do');
  @Input() activities: any = [];
  @Input() events: any = [];

  constructor(
    private bottomSheet: MatBottomSheet,
    private eventService: EventService,
    private alertService: AlertService,
  ) { }

  ngOnInit() {

  }


  openBottomSheet(events, activities): void {
    this.bottomSheet.open(BottomBarComponent, {
      data: { events, activities },
    });
  }
  stickerClicked(eventId: number) {

    this.eventService.addNew({ eventId } as Event)
      .pipe(first())
      .subscribe(
        (data: any) => {
          this.alertService.success('event added successfully', true);
          this.events.push(data.newEvent[0]);
          for (let i = 0; i < this.activities.length; i++) {
            if (data.newEvent[0].id === this.activities[i].id) {
              this.activities.splice(i, 1);
            }

          }
        },
        error => {
          this.alertService.error(error);
        });
  }



}


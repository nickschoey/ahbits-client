﻿import { Component, OnInit } from '@angular/core';
import { first } from 'rxjs/operators';
import { User, Activity } from '../_models';
import { EventService, ActivityService, CalendarService } from '../_services';
import { MatDialog, MatDialogRef } from '@angular/material';
import { MatBottomSheet } from '@angular/material';
import { DialogComponent } from '../dialog/dialog.component';
import * as moment from 'moment';
import { BottomBarComponent } from '../bottom-bar/bottom-bar.component';

@Component({
    templateUrl: 'home.component.html',
    styleUrls: ['./home.component.css'],
}
)

export class HomeComponent implements OnInit {
    dialogRef: MatDialogRef<DialogComponent>;
    activities: any = [];
    allEvents: any = [];
    allEventsIndex: any = [];
    todayEvents: any = [];
    myCalendar: Array<any>;
    calendarIndex: Array<string>;

    constructor(
        private bottomSheet: MatBottomSheet,
        private dialog: MatDialog,
        private eventService: EventService,
        private activityService: ActivityService,
        private calendarService: CalendarService
    ) {
    }
    ngOnInit() {
        this.loadPastEvents();
        this.loadTodayEvents();
        this.loadRemainingActivities();
        this.loadCalendar();
        console.log();

    }


    // handleActivityCreated(activity: Activity) {
    //     this.activities = [... this.activities, activity];
    // }

    private loadCalendar() {
        this.calendarService.getOrigin(JSON.parse(localStorage.currentUser).createdAt)
            .pipe(first())
            .subscribe((calendar: Array<any>) => {

                this.myCalendar = calendar.reduce((acc, el) => {

                    acc[el.db_date] = el;
                    return acc;
                }, {});
                console.log(this.myCalendar);

                this.calendarIndex = Object.keys(this.myCalendar).sort();
            });
    }
    private loadPastEvents() {
        this.eventService.getPast()
            .pipe(first())
            .subscribe((events: Array<any>) => {
                this.allEvents = events.reduce((accum, el) => {
                    const date = moment(el.createdAt).format('YYYY-MM-DD');
                    if (!accum[date]) { accum[date] = []; }
                    accum[date].push(el);
                    return accum;
                }, {});

                this.allEventsIndex = Object.keys(this.allEvents).sort();
            });
    }

    private loadRemainingActivities() {
        this.activityService.getRemaining()
            .pipe(first())
            .subscribe(activities => {
                this.activities = activities;
            });
    }

    private loadTodayEvents() {
        this.eventService.getToday()
            .pipe(first())
            .subscribe((events: Array<any>) => {
                this.todayEvents = events.sort();
            });
    }

    openAddActivityDialog() {
        this.dialogRef = this.dialog.open(DialogComponent);
        this.dialogRef.afterClosed()
            .subscribe(result => {
                this.loadPastEvents();
                if (result) {
                    this.activities.push(result);
                }

            });
    }

    openBottomSheet(events, activities): void {
        this.bottomSheet.open(BottomBarComponent, {
            data: { events, activities },
        });
    }
}

import { Component, OnInit, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { AlertService } from '../_services';
import { ActivityService } from '../_services/activity.service';
import { first } from 'rxjs/operators';
import { Activity } from '../_models';


export interface Color {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-activity',
  templateUrl: './activity.component.html',
  styleUrls: ['./activity.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class ActivityComponent implements OnInit {
  activities: any = [];
  loading: false;
  @Output()
  // activityCreated: EventEmitter<Activity> = new EventEmitter<Activity>();

  colors: Color[] = [
    { value: 'red', viewValue: 'Red' },
    { value: 'blue', viewValue: 'Blue' },
    { value: 'green', viewValue: 'Green' },
    { value: 'yellow', viewValue: 'Yellow' },
    { value: 'purple', viewValue: 'Purple' },
    { value: 'black', viewValue: 'Black' },
    { value: 'orange', viewValue: 'Orange' },
  ];
  activityForm = new FormGroup({
    activityName: new FormControl(''),
    stickerColor: new FormControl(''),
    description: new FormControl('')
  });
  submitted = false;
  panelOpenState = false;

  constructor(
    private activityService: ActivityService,
    private alertService: AlertService
  ) { }

  ngOnInit() {
    this.loadAllActivities();
  }

  onSubmit() {
    this.submitted = true;

    // stop here if form is invalid
    if (this.activityForm.invalid) {
      return;
    }

    // Injects the id of the currentuser into the data
    // sent to the server
    // this.activityForm.value.id = JSON.parse(localStorage.currentUser).id;
    console.log(this.activityForm.value);

    this.activityService.addNew(this.activityForm.value)
      .pipe(first())
      .subscribe(
        data => {
          this.alertService.success('Activity added successfully', true);
          this.activities.push(data);
          // this.activityCreated.emit(new Activity(data as Activity));
        },
        error => {
          this.alertService.error(error);
        });
    this.activityForm.reset();

  }

  loadAllActivities() {
    this.activityService.getAll()
      .pipe(first())
      .subscribe(activities => {
        this.activities = activities;

      });

  }

  togglePanel() {
    this.panelOpenState = !this.panelOpenState;
    
  }

}

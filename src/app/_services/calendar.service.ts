import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable()
export class CalendarService {

  constructor(
    private http: HttpClient
  ) { }

  getOrigin(creationDate) {
    return this.http.get(`${environment.apiUrl}/calendar/origin/${creationDate}`);
  }


  getToday() {
    return this.http.get(`${environment.apiUrl}/calendar/today`);
  }


}


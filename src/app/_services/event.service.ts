import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Event } from '../_models';

@Injectable()
export class EventService {

  constructor(private http: HttpClient) { }

  addNew(event: Event) {
    return this.http.post(`${environment.apiUrl}/events`, event);
  }

  getToday() {
    return this.http.get(`${environment.apiUrl}/events/today`);

  }
  getPast() {
    return this.http.get(`${environment.apiUrl}/events/past`);
  }

  getOrigin(creationDate) {
    return this.http.get(`${environment.apiUrl}/calendar/origin/${creationDate}`);
  }

}

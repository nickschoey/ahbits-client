import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Activity } from '../_models';


@Injectable()
export class ActivityService {

  constructor(private http: HttpClient) { }

  addNew(activity: Activity) {
    return this.http.post(`${environment.apiUrl}/activity/add`, activity);
  }

  getAll() {
    return this.http.get(`${environment.apiUrl}/activity`);
  }

  getRemaining() {
    return this.http.get(`${environment.apiUrl}/activity/remaining`);
  }
}

import { Component, OnInit, ViewEncapsulation, Output, EventEmitter } from '@angular/core';
import { MatDialogRef } from '@angular/material';
import { FormGroup, FormControl } from '@angular/forms';
import { AlertService } from '../_services';
import { ActivityService } from '../_services/activity.service';
import { first } from 'rxjs/operators';
import { Activity } from '../_models';
export interface Color {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class DialogComponent implements OnInit {

  colors: Color[] = [
    { value: 'red', viewValue: 'Red' },
    { value: 'blue', viewValue: 'Blue' },
    { value: 'green', viewValue: 'Green' },
    { value: 'yellow', viewValue: 'Yellow' },
    { value: 'purple', viewValue: 'Purple' },
    { value: 'black', viewValue: 'Black' },
    { value: 'orange', viewValue: 'Orange' },
  ];
  activityForm = new FormGroup({
    activityName: new FormControl(''),
    stickerColor: new FormControl('')
  });
  submitted = false;
  constructor(
    private activityService: ActivityService,
    private alertService: AlertService,
    private dialogRef: MatDialogRef<DialogComponent>
  ) { }

  ngOnInit() {
  }
  submit() {
    this.activityService.addNew(this.activityForm.value)
    .pipe(first())
    .subscribe(
      data => {
        this.alertService.success('Activity added successfully', true);
        this.dialogRef.close(data);
        },
        error => {
          this.alertService.error(error);
        });
    this.activityForm.reset();
  }

}

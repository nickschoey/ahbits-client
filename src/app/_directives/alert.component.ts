﻿import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { MatSnackBar } from '@angular/material';
import { AlertService } from '../_services';


// The alert component passes alert messages to the template whenever a message
// is received from the alert service. It does this by subscribing to the alert
// service's getMessage() method which returns an Observable.

@Component({
    selector: 'app-alert',
    templateUrl: 'alert.component-mock.html'
})

export class AlertComponent implements OnInit, OnDestroy {
    private subscription: Subscription;
    message: any;
    
    constructor(
        private alertService: AlertService,
        public snackBar: MatSnackBar
    ) { }
    
    ngOnInit() {
        this.subscription = this.alertService.getMessage().subscribe(message => {
            this.message = message;
            if (this.message) {
                this.snackBar.open(this.message.text, '', {
                    duration: 2000,
                });
            }
        });
    }

    ngOnDestroy() {
        this.subscription.unsubscribe();
    }
    

}

@Component({
    selector: 'app-alert-snack',
    templateUrl: 'alert.component.html',
})
export class AlertSnackComponent { 
    message: any;

}

import { Component, Input } from '@angular/core';
import { MatBottomSheet } from '@angular/material';
import { BottomBarComponent } from '../bottom-bar/bottom-bar.component';
@Component({
  selector: 'app-day',
  templateUrl: './day.component.html',
  styleUrls: ['./day.component.css']
})
export class DayComponent {

  @Input() activitiesToDo: any[];
  @Input() allEvents: any[];
  @Input() day: any;

  constructor(
    private bottomSheet: MatBottomSheet,
  ) { }

  openBottomSheet(events, day): void {
    this.bottomSheet.open(BottomBarComponent, {
      data: { events, day },
    });

  }

}


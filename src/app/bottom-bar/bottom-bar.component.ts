import { Component, Inject, OnInit } from '@angular/core';
import { MatBottomSheetRef } from '@angular/material';
import { MAT_BOTTOM_SHEET_DATA } from '@angular/material';
import * as moment from 'moment';
@Component({
  selector: 'app-bottom-bar',
  templateUrl: 'bottom-bar.component.html',
  styleUrls: ['./bottom-bar.component.css']
})
export class BottomBarComponent implements OnInit {
  date: String;


  constructor(
    private bottomSheetRef: MatBottomSheetRef<BottomBarComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public data: any
  ) { }
  ngOnInit(): void {
    this.date = moment(this.data.events[0].createdAt).format('dddd Do MMMM YYYY');

  }
}


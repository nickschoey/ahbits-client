﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppComponent } from './app.component';
import { routing } from './app.routing';
import { MaterialModule } from './material/material.module';
import { AlertComponent, AlertSnackComponent } from './_directives/alert.component';
import { AuthGuard } from './_guards';
import { JwtInterceptor, ErrorInterceptor } from './_helpers';
import { AlertService, AuthenticationService, UserService, ActivityService, EventService, CalendarService } from './_services';
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { SplashComponent } from './splash/splash.component';
import { ActivityComponent } from './activity/activity.component';
import { DayComponent } from './day/day.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { DialogComponent } from './dialog/dialog.component';
import { TodayComponent } from './today/today.component';
import { TestComponent, PizzaPartyComponent } from './test/test.component';
import { BottomBarComponent } from './bottom-bar/bottom-bar.component';

import { NewuserComponent } from './newuser/newuser.component';
@NgModule({
    imports: [
        MaterialModule,
        BrowserModule,
        BrowserAnimationsModule,
        ReactiveFormsModule,
        HttpClientModule,
        routing
    ],
    declarations: [
        AppComponent,
        AlertComponent,
        HomeComponent,
        LoginComponent,
        RegisterComponent,
        SplashComponent,
        ActivityComponent,
        DayComponent,
        ToolbarComponent,
        DialogComponent,
        TodayComponent,
        TestComponent,
        AlertSnackComponent,
        PizzaPartyComponent,
        BottomBarComponent,
        NewuserComponent,
    ],
    providers: [
        AuthGuard,
        ActivityService,
        AlertService,
        AuthenticationService,
        UserService,
        EventService,
        CalendarService,
        { provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true },
        { provide: HTTP_INTERCEPTORS, useClass: ErrorInterceptor, multi: true },

    ],
    bootstrap: [AppComponent],
    entryComponents: [DialogComponent, BottomBarComponent, AlertSnackComponent]
})

export class AppModule { }



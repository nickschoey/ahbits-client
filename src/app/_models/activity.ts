export class Activity {
  id: number;
  name: string;
  color: string;
  currentStreak: number;
  highestStreak: number;

  constructor({id, name, color, currentStreak, highestStreak}) {
    this.id = id;
    this.name = name;
    this.color = color;
    this.currentStreak = currentStreak;
    this.highestStreak = highestStreak;
  }
}
